const multer = require('multer');
const crypto = require('crypto');
const { extname, resolve } = require('path');

module.exports = app => {
  const avatarUploadConfig = {
    storage: multer.diskStorage({
      destination: resolve(__dirname, '..', 'uploads', 'avatars'),
      filename: (req, file, cb) => {
        crypto.randomBytes(16, (err, res) => {
          if (err) return cb(err);
          const allowedExtensions = ['.jpg', '.jpeg', '.png'];

          const isAllowedExtension = allowedExtensions.includes(
            extname(file.originalname).toLocaleLowerCase()
          );

          if (!isAllowedExtension)
            return cb(new Error('Formato de arquivo não aceito!'));

          return cb(null, res.toString('hex') + extname(file.originalname));
        });
      },
    }),
  };

  const taskUploadConfig = {
    storage: multer.diskStorage({
      destination: resolve(__dirname, '..', 'uploads', 'taskFiles'),
      filename: (req, file, cb) => {
        crypto.randomBytes(16, (err, res) => {
          if (err) return cb(err);
          const allowedExtensions = ['.zip', '.rar', '.pdf', '.txt', '.jpg', '.jpeg', '.png']

          const isAllowedExtension = allowedExtensions.includes(
            extname(file.originalname).toLocaleLowerCase(),
          )

          if (!isAllowedExtension) {
            return cb(new Error('Formato de arquivo não é aceito'))
          }
          return cb(null, res.toString('hex') + extname(file.originalname))


        })
      }
    })
  }

  const avatarUpload = multer(avatarUploadConfig);

  const taskUploads = multer(taskUploadConfig)

  return { avatarUpload, taskUploads };
};
