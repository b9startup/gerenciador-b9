const multer = require('multer');

module.exports = app => {
  const { checkAdmin, checkMembers } = app.middlewares;
  const { avatarUpload, taskUploads } = app.config.multer;

  app.post('/v1/sessions', app.validators.signin, app.api.auth.signin);

  app
    .route('/v1/usuarios')
    .all(app.config.passport.authenticate())
    .post(checkAdmin, app.validators.userSignUp, app.api.user.signUp)
    .put(app.validators.userUpdate, app.api.user.update);

  app
    .route('/v1/projetos')
    .all(app.config.passport.authenticate())
    .post(checkAdmin, app.validators.projectStore, app.api.projects.store)
    .get(app.api.projects.index);

  app
    .route('/v1/projetos/:id')
    .all(app.config.passport.authenticate())
    .get(checkMembers, app.api.projects.show)
    .put(checkAdmin, app.validators.projectUpdate, app.api.projects.update)
    .delete(checkAdmin, app.api.projects.destroy);

  app
    .route('/v1/projeto/:project_id/membro/:user_id')
    .all(app.config.passport.authenticate())
    .post(checkAdmin, app.api.projectMember.store)
    .delete(checkAdmin, app.api.projectMember.destroy);

  app
    .route('/v1/projeto/:project_id/membros')
    .all(app.config.passport.authenticate())
    .get(app.api.projectMember.index);

  app
    .route('/v1/projeto/:project_id/historia')
    .all(app.config.passport.authenticate())
    .post(checkMembers, app.validators.storys, app.api.storys.store)
    .get(checkMembers, app.api.storys.index);

  app
    .route('/v1/projeto/:project_id/historia/:story_id')
    .all(app.config.passport.authenticate())
    .get(checkMembers, app.api.storys.show)
    .put(checkMembers, app.validators.storys, app.api.storys.update)
    .delete(checkMembers, app.api.storys.destroy);

  app
    .route('/v1/projeto/:project_id/historia/:story_id/tasks')
    .all(app.config.passport.authenticate())
    .post(checkMembers, app.validators.tasks, app.api.tasks.store)
    .get(checkMembers, app.api.tasks.index);

  app
    .route('/v1/projeto/:project_id/historia/:story_id/tasks/:task_id')
    .all(app.config.passport.authenticate())
    .get(checkMembers, app.api.tasks.show)
    .put(checkMembers, app.validators.taskUpdate, app.api.tasks.update)
    .delete(checkMembers, app.api.tasks.destroy);

  app
    .route('/v1/projeto/:project_id/historia/:story_id/tasks/:task_id/historico')
    .all(app.config.passport.authenticate())
    .get(checkMembers, app.api.historicTasks.index);

  app
    .route('/v1/projeto/:project_id/historia/:story_id/tasks/:task_id/comentarios')
    .all(app.config.passport.authenticate())
    .post(checkMembers, app.validators.commentsTasks, app.api.commentsTasks.store)
    .get(checkMembers, app.api.commentsTasks.index);

  app
    .route('/v1/projeto/:project_id/historia/:story_id/tasks/:task_id/comentarios/:comment_id')
    .all(app.config.passport.authenticate())
    .put(checkMembers, app.validators.commentsTasks, app.api.commentsTasks.update)
    .delete(checkMembers, app.api.commentsTasks.destroy);

  app
    .route('/v1/projeto/:project_id/historia/:story_id/tasks/:task_id/files')
    .all(app.config.passport.authenticate())
    .post(checkMembers, taskUploads.array('files', 5), app.api.taskFiles.store)
    .get(checkMembers, app.api.taskFiles.index)

  app
    .route('/v1/projeto/:project_id/historia/:story_id/tasks/:task_id/files/:file_id')
    .all(app.config.passport.authenticate())
    .delete(checkMembers, app.api.taskFiles.destroy)

  app
    .route('/v1/avatars')
    .all(app.config.passport.authenticate())
    .post(avatarUpload.single('file'), app.api.file.store);
};

