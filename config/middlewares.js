const express = require('express');
const { resolve } = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');

module.exports = app => {
  app.use(bodyParser.json());
  app.use(
    '/avatars',
    express.static(resolve(__dirname, '..', 'uploads', 'avatars'))
  );

  app.use(
    '/v1/projeto/:project_id/historia/:story_id/tasks/:task_id/files',
    express.static(resolve(__dirname, '..', 'uploads', 'taskFiles'))
  )
  app.use(cors());
};
