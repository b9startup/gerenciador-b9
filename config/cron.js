const { CronJob } = require('cron');

module.exports = app => {
  const clearSprints = new CronJob(
    '0 0 6 * * *',
    app.jobs.clearSprint,
    null,
    true,
    'America/Sao_Paulo'
  );

  return { clearSprints };
};
