const nest = require('nested-knex');

const schema = {
  id: nest.number('projects.id', { id: true }),
  name: nest.string('projects.name'),
  description: nest.string('projects.description'),
  starting_date: nest.date('projects.starting_date'),
  expected_end_date: nest.date('projects.expected_end_date'),
  delivery_date: nest.date('projects.delivery_date'),
  expected_hours: nest.number('projects.expected_hours'),
  time_spent: nest.number('projects.time_spent'),
  requester_id: nest.number('projects.requester_id'),
  requester: nest.type({
    id: nest.number('users.id'),
    name: nest.string('users.name'),
  }),
  requirements: nest.string('projects.requirements'),
  status: nest.string('projects.status'),
  sprint_days_interval: nest.string('projects.sprint_days_interval'),
  sprint: nest.type({
    id: nest.number('sprints.id'),
    name: nest.string('sprints.name'),
    project_id: nest.number('sprints.project_id'),
    starting_date: nest.date('sprints.starting_date'),
    expiration_date: nest.date('sprints.expiration_date'),
    histories: nest.array(
      nest.type({
        id: nest.number('story.id'),
        name: nest.string('story.name'),
        status: nest.string('story.status'),
        tasks: nest.array(
          nest.type({
            id: nest.number('tasks.id'),
            name: nest.string('tasks.name'),
            description: nest.string('tasks.description'),
            situation: nest.string('tasks.situation'),
            user_assigned: nest.type({
              id: nest.number('user_assigned.id'),
              name: nest.string('user_assigned.name'),
            }),
            expected_start_date: nest.date('tasks.expected_start_date'),
            expected_end_date: nest.date('tasks.expected_end_date'),
            expected_hours: nest.string('tasks.expected_hours'),
            time_spent: nest.string('tasks.time_spent'),
            start_date: nest.string('tasks.start_date'),
            end_date: nest.string('tasks.end_date'),
          })
        ),
      })
    ),
  }),
};

module.exports = schema;
