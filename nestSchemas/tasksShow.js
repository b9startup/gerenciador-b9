const nest = require('nested-knex')


const schema = {
    id: nest.number('tasks.id', { id: true }),
    name: nest.string('tasks.name'),
    situation: nest.string('tasks.situation'),
    story_id: nest.number('tasks.story_id'),
    sprint_id: nest.number('tasks.sprint_id'),
    expected_start_date: nest.date('tasks.expected_start_date'),
    expected_end_date: nest.date('tasks.expected_end_date'),
    expected_hours: nest.string('tasks.expected_hours'),
    time_spent: nest.number('tasks.time_spent'),
    description: nest.string('tasks.description'),
    start_date: nest.date('tasks.start_date'),
    end_date: nest.date('tasks.end_date'),
    user_attributed: nest.type({
        id: nest.number('users.id'),
        name: nest.string('users.name')
    }),
    comments: nest.array(
        nest.type({
            id: nest.number('comments.id'),
            comment: nest.string('comments.comments'),
            users_assigned: nest.type({
                id: nest.number('users_assigned.id'),
                name: nest.string('users_assigned.name')
            })
        })
    ),
    files: nest.array(
        nest.type({
            id: nest.number('task_files.id'),
            name: nest.string('task_files.original_name')
        })
    )

}


module.exports = schema