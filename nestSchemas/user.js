const nest = require('nested-knex');

const schema = {
  id: nest.number('users.id'),
  password: nest.string('users.password'),
  name: nest.string('users.name'),
  email: nest.string('users.email'),
  admin: nest.string('users.admin'),
  avatar: nest.type({
    id: nest.number('files.id'),
    file: nest.string('files.name'),
  }),
};

module.exports = schema;
