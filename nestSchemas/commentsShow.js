const nest = require('nested-knex')

const schema = {
    id: nest.number('comments.id', { id: true }),
    comments: nest.string('comments.comments'),
    user: nest.type({
        id: nest.number('users.id', { id: true }),
        name: nest.string('users.name'),
    })


}


module.exports = schema