const Yup = require('yup');

const schema = Yup.object().shape({
  name: Yup.string().required('O nome da historia é Obrigatório'),
});

module.exports = app => {
  const validate = async (req, res, next) => {
    try {
      await schema.validate(req.body, { abortEarly: false });

      next();
    } catch (err) {
      res.status(400).send(err);
    }
  };

  return validate;
};
