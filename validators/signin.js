const yup = require('yup');

const schema = yup.object().shape({
  email: yup.string().required('O e-mail é obrigatório!'),
  password: yup.string().required('A senha é obrigatória!'),
});

module.exports = app => {
  const validate = async (req, res, next) => {
    try {
      await schema.validate(req.body, { abortEarly: false });

      next();
    } catch (error) {
      res.status(400).send(error.inner);
    }
  };

  return validate;
};
