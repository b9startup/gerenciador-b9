const yup = require('yup');

const schema = yup.object().shape({
  name: yup.string(),
  email: yup.string().email('Formato de e-mail inválido!'),
  old_password: yup.string(),
  password: yup.string().when('old_password', (old_password, schema) => {
    return (
      old_password &&
      schema
        .required('Você precisa informar a nova senha para prosseguir.')
        .min(8, 'Sua senha deve conter ao menos 8 caracteres!')
    );
  }),
  password_confirmation: yup.string().when('password', (password, schema) => {
    return (
      password &&
      schema
        .required('A confirmação de senha é obrigatória.')
        .oneOf([password], 'As senhas não coincidem!')
    );
  }),
});

module.exports = app => {
  const validate = async (req, res, next) => {
    try {
      await schema.validate(req.body, { abortEarly: false });

      next();
    } catch (error) {
      res.status(400).send(error.inner);
    }
  };

  return validate;
};
