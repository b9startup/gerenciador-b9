const Yup = require('yup');

const schema = Yup.object().shape({
  name: Yup.string().required('O Nome da tarefa é obrigatorio'),
  expected_start_date: Yup.date('ANO-MES-DIA').required(
    'A expectativa de data de inicio é Obrigatoria'
  ),
  expected_end_date: Yup.date('ANO-MES-DIA').required(
    'A expectativa de data de finalização é obrigatoria'
  ),
  expected_hours: Yup.string().required(
    'A expectativa de horas gastas é obrigatoria'
  ),
  description: Yup.string().required('A descrição da tarefa é obrigatorio'),
});

module.exports = app => {
  const validate = async (req, res, next) => {
    try {
      await schema.validate(req.body, { abortEarly: false });

      next();
    } catch (err) {
      res.status(400).send(err);
    }
  };

  return validate;
};
