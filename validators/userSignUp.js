const yup = require('yup');

const schema = yup.object().shape({
  name: yup.string().required('O nome é obrigatório!'),
  email: yup
    .string()
    .email('Formato de e-mail inválido!')
    .required('O e-mail é obrigatório!'),
  password: yup
    .string()
    .required('A senha é obrigatória!')
    .min(8, 'Sua senha deve conter ao menos 8 caracteres!'),
  password_confirmation: yup.string().when('password', (password, schema) => {
    return (
      password &&
      schema
        .required('A confirmação de senha é obrigatória.')
        .oneOf([password], 'As senhas não coincidem!')
    );
  }),
});

module.exports = app => {
  const validate = async (req, res, next) => {
    try {
      await schema.validate(req.body, { abortEarly: false });

      next();
    } catch (error) {
      res.status(400).send(error.inner);
    }
  };

  return validate;
};
