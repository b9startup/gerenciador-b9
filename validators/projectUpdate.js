const yup = require('yup');

const schema = yup.object().shape({
  name: yup.string().required('O nome é obrigatório!'),
  starting_date: yup.date().required('A data de início é obrigatória!'),
  expected_end_date: yup
    .date()
    .required('A data final prevista é obrigatória!'),
  expected_hours: yup.string().required('A estimativa de horas é obrigatória!'),
  priority: yup.string().required('A prioridade é obrigatória!'),
  sprint_days_interval: yup
    .number('O intervalo de dias deve ser um número inteiro!')
    .min(1, 'O número de dias mínimo entre sprints é 1.')
    .required('O intervalo de dias entre sprints é obrigatório!'),
});

module.exports = app => {
  const validate = async (req, res, next) => {
    try {
      await schema.validate(req.body, { abortEarly: false });

      next();
    } catch (error) {
      res.status(400).send(error.inner);
    }
  };

  return validate;
};
