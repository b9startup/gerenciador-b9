const Yup = require('yup');

const schema = Yup.object().shape({
  comments: Yup.string().required('O Comentario é Obrigatorio'),
});

module.exports = app => {
  const validate = async (req, res, next) => {
    try {
      await schema.validate(req.body, { abortEarly: false });

      next();
    } catch (err) {
      res.status(400).send(err);
    }
  };

  return validate;
};
