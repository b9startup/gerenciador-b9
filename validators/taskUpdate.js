const Yup = require('yup');

const schema = Yup.object().shape({
  name: Yup.string(),
  expected_start_date: Yup.date('ANO-MES-DIA'),
  expected_end_date: Yup.date('ANO-MES-DIA'),
  expected_hours: Yup.string(),
  description: Yup.string(),
  situation: Yup.string(
    'A situação deve ser: Nova, Em Analise, Desenvolvimento, Parado, Aguardando Testes, Testando, Aprovado, Reprovado'
  ),
  user_id: Yup.number('ID do usuario a ser atribuido'),
  sprint_id: Yup.number('ID da Sprint que deseja levar a tarefa'),
});

module.exports = app => {
  const validate = async (req, res, next) => {
    try {
      await schema.validate(req.body, { abortEarly: false });

      next();
    } catch (err) {
      res.status(400).send(err);
    }
  };

  return validate;
};
