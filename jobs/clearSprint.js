const { format, addDays } = require('date-fns');
const pt = require('date-fns/locale/pt');

module.exports = app => {
  const clearSprints = async () => {
    const sprints = await app.db
      .select('*')
      .from('sprints')
      .where('current_sprint', true);

    await Promise.all(
      sprints.map(async sprint => {
        const { id, project_id } = sprint;

        const formattedExpirationDate = format(
          sprint.expiration_date,
          'yyyy-MM-dd'
        );
        const formattedCurrentDate = format(new Date(), 'yyyy-MM-dd');

        if (formattedExpirationDate === formattedCurrentDate) {
          try {
            app.db.transaction(async trx => {
              await trx('sprints')
                .where({ id })
                .update('current_sprint', false);

              const project = await trx('projects')
                .where('id', project_id)
                .first();

              await trx('sprints').insert({
                name: format(new Date(), "d 'de' MMMM 'de' yyyy", {
                  locale: pt,
                }),
                project_id,
                current_sprint: true,
                starting_date: new Date(),
                expiration_date: addDays(
                  new Date(),
                  project.sprint_days_interval
                ),
              });
            });
          } catch (error) {
            console.error(error);
          }
        }
      })
    );
  };

  return clearSprints;
};
