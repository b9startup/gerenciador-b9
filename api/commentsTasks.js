const nest = require('nested-knex')
const commentsShow = require('../nestSchemas/commentsShow')

module.exports = app => {
    const store = async (req, res) => {
        const { task_id } = req.params
        const { comments } = req.body

        try {
            const comment = await app
                .db('comments').returning('*')
                .insert({ task_id, user_id: req.user.id, comments })
            return res.send(comment)
        } catch (err) {
            return res.status(400).send(err)
        }

    }

    const index = async (req, res) => {
        const { task_id } = req.params

        try {
            const comments = await nest.array(nest.type(commentsShow)).withQuery(
                app
                    .db('comments')
                    .select('*')
                    .where({ ['comments.task_id']: task_id })
                    .leftJoin('users', 'comments.user_id', 'users.id')
                    .orderBy('comments.id')
            )



            return res.send(comments)
        } catch (err) {
            return res.status(400).send(err)
        }

    }

    const update = async (req, res) => {
        const { task_id, comment_id } = req.params
        const { comments } = req.body

        try {
            const comment = await app
                .db('comments')
                .where({ id: comment_id, task_id, user_id: req.user.id })
                .update({ comments })
                .returning('*')
            return res.send(comment)
        } catch (err) {
            return res.status(400).send(err)
        }
    }

    const destroy = async (req, res) => {
        const { task_id, comment_id } = req.params

        try {
            await app.db('comments').where({ id: comment_id, task_id, user_id: req.user.id }).del()
            return res.send("Deletado com Sucesso")

        } catch (err) {
            return res.status(400).send(err)
        }

    }

    return { store, index, update, destroy }
}