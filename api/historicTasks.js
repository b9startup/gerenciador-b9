module.exports = app => {
    const index = async (req, res) => {
        const { task_id } = req.params

        try {
            const history = await app
                .db('task_history')
                .select('*')
                .where({ id_task: task_id })
            return res.send(history)
        } catch (err) {
            return res.status(400).send(err)

        }

    }

    return { index }
}