const { authSecret } = require('../.env');
const nest = require('nested-knex');
const jwt = require('jwt-simple');
const bcrypt = require('bcryptjs');

const userSchema = require('../nestSchemas/user');

module.exports = app => {
  const signin = async (req, res) => {
    const user = await app
      .db('users')
      .where({ email: req.body.email })
      .first();

    if (!user) return res.status(400).send('Usuário não encontrado');

    const isMatch = await bcrypt.compare(req.body.password, user.password);
    if (!isMatch) return res.status(401).send('Email e/ou Senha inválidos');

    const now = Math.floor(Date.now() / 1000);

    const userInfo = await nest.type(userSchema).withQuery(
      app
        .db('users')
        .where({ email: req.body.email })
        .leftJoin('files', 'users.avatar_id', 'files.id')
        .first()
    );

    const payload = {
      id: userInfo.id,
      name: userInfo.name,
      email: userInfo.email,
      admin: userInfo.admin,
      avatar: {
        id: userInfo.avatar.id,
        file: userInfo.avatar.file,
      },
      iat: now,
      exp: now + 60 * 60 * 24 * 2,
    };

    res.json({
      ...payload,
      token: jwt.encode(payload, authSecret),
    });
  };

  const validateToken = async (req, res) => {
    const userDate = req.body || null;

    try {
      if (userDate) {
        const token = jwt.decode(userDate.token, authSecret);
        if (new Date(token.exp * 1000) > new Date()) {
          return res.send(true);
        }
      }
    } catch (e) {
      //Problema com Token
    }
    res.send(false);
  };
  return { signin, validateToken };
};
