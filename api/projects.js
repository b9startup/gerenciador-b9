const { format, addDays, parseISO } = require('date-fns');
const pt = require('date-fns/locale/pt');
const nest = require('nested-knex');

const projectShow = require('../nestSchemas/projectShow');

module.exports = app => {
  const store = async (req, res) => {
    const {
      name,
      description,
      starting_date,
      expected_end_date,
      delivery_date,
      expected_hours,
      time_spent,
      priority,
      requirements,
      status,
      sprint_days_interval,
    } = req.body;

    app.db.transaction(async trx => {
      try {
        const project = await trx('projects')
          .returning('*')
          .insert({
            name,
            description,
            starting_date,
            expected_end_date,
            delivery_date,
            expected_hours,
            time_spent,
            priority,
            requester_id: req.user.id,
            requirements,
            status,
            sprint_days_interval,
          });

        await trx('project_members').insert({
          user_id: req.user.id,
          project_id: project[0].id,
        });

        await trx('sprints').insert({
          name: format(parseISO(starting_date), "d 'de' MMMM 'de' yyyy", {
            locale: pt,
          }),
          project_id: project[0].id,
          current_sprint: true,
          starting_date,
          expiration_date: addDays(
            parseISO(starting_date),
            sprint_days_interval
          ),
        });

        await trx.commit();

        res.status(201).send(project[0]);
      } catch (error) {
        await trx.rollback(error);

        res.send(error);
      }
    });
  };

  const index = async (req, res) => {
    try {
      const projects = await app.db.select('*').from('projects');

      res.send(projects);
    } catch (error) {
      res.send(error);
    }
  };

  const show = async (req, res) => {
    const { id } = req.params;

    try {
      const project = await nest.type(projectShow).withQuery(
        app
          .db('projects')
          .select('*')
          .where('projects.id', id)
          .leftJoin('users', 'projects.requester_id', 'users.id')
          .leftJoin('sprints', function() {
            this.on('projects.id', 'sprints.project_id').onIn(
              'sprints.current_sprint',
              true
            );
          })
          .leftJoin('story', 'sprints.id', 'story.sprint_id')
          .leftJoin('tasks', 'story.id', 'tasks.story_id')
          .leftJoin(
            'users as user_assigned',
            'tasks.user_id',
            'user_assigned.id'
          )
      );

      res.send(project);
    } catch (error) {
      console.log(error);
      res.send(error);
    }
  };

  const update = async (req, res) => {
    const { id } = req.params;
    const {
      name,
      description,
      starting_date,
      expected_end_date,
      delivery_date,
      expected_hours,
      time_spent,
      priority,
      requirements,
      status,
      sprint_days_interval,
    } = req.body;

    try {
      const project = await app
        .db('projects')
        .where({ id })
        .update(
          {
            name,
            description,
            starting_date,
            expected_end_date,
            delivery_date,
            expected_hours,
            time_spent,
            priority,
            requirements,
            status,
            sprint_days_interval,
          },
          '*'
        );

      res.send(project);
    } catch (error) {
      res.send(error);
    }
  };

  const destroy = async (req, res) => {
    const { id } = req.params;

    try {
      await app
        .db('projects')
        .where({ id })
        .del();

      res.send('Projeto deletado com sucesso!');
    } catch (error) {
      res.send(error);
    }
  };

  return { store, index, show, update, destroy };
};
