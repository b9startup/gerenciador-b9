module.exports = app => {
  const store = async (req, res) => {
    const { project_id, user_id } = req.params;

    const projectExists = await app
      .db('projects')
      .where('id', project_id)
      .first();

    if (!projectExists) return res.status(404).send('Projeto não encontrado!');

    const userExists = await app
      .db('users')
      .where('id', user_id)
      .first();

    if (!userExists) return res.status(404).send('Usuário não encontrado!');

    const isAlreadyMember = await app
      .db('project_members')
      .where({ project_id })
      .andWhere({ user_id })
      .first();

    if (isAlreadyMember)
      return res.status(400).send('Este usuário já é faz parte do projeto.');

    try {
      const member = await app
        .db('project_members')
        .returning('*')
        .insert({ project_id, user_id });

      res.send(member[0]);
    } catch (error) {
      res.send(error);
    }
  };

  const index = async (req, res) => {
    const { project_id } = req.params;

    const projectExists = await app
      .db('projects')
      .where('id', project_id)
      .first();

    if (!projectExists) return res.status(404).send('Projeto não encontrado!');

    try {
      const members = await app
        .db('project_members')
        .select([
          'project_members.id',
          'project_members.user_id',
          'project_members.project_id',
          'users.id',
          'users.name',
          'users.email',
        ])
        .where({ project_id })
        .innerJoin('users', 'project_members.user_id', 'users.id')
        .orderBy('project_members.id', 'asc');

      res.send(members);
    } catch (error) {
      res.send(error);
    }
  };

  const destroy = async (req, res) => {
    const { project_id, user_id } = req.params;

    const projectExists = await app
      .db('projects')
      .where('id', project_id)
      .first();

    if (!projectExists) return res.status(404).send('Projeto não encontrado!');

    const userExists = await app
      .db('users')
      .where('id', user_id)
      .first();

    if (!userExists) return res.status(404).send('Usuário não encontrado!');

    const isMemberOfProject = await app
      .db('project_members')
      .where({ project_id })
      .andWhere({ user_id })
      .first();

    if (!isMemberOfProject)
      return res
        .status(400)
        .send('O usuário informado não faz parte do projeto.');

    try {
      await app
        .db('project_members')
        .where({ project_id })
        .andWhere({ user_id })
        .del();

      res.send('O usuário foi removido do projeto com sucesso!');
    } catch (error) {
      res.send(error);
    }
  };

  return { store, index, destroy };
};
