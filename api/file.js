module.exports = app => {
  const store = async (req, res) => {
    const { originalname: original_name, filename: name } = req.file;

    try {
      const file = await app
        .db('files')
        .returning('*')
        .insert({ name, original_name });

      res.send(file[0]);
    } catch (error) {
      res.send(error);
    }
  };

  return { store };
};
