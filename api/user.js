const bcrypt = require('bcryptjs');
const nest = require('nested-knex');

const userSchema = require('../nestSchemas/user');

module.exports = app => {
  const signUp = async (req, res) => {
    const { name, email, password, admin } = req.body;

    const emailAlreadyUsed = await app
      .db('users')
      .where({ email })
      .first();

    if (emailAlreadyUsed)
      return res.status(400).send('O e-mail já está em uso!');

    const hashed_password = await bcrypt.hash(password, 10);

    try {
      const userId = await app
        .db('users')
        .returning('id')
        .insert({ name, email, password: hashed_password, admin });

      const user = await nest.type(userSchema).withQuery(
        app
          .db('users')
          .select('*')
          .where('users.id', userId[0])
          .leftJoin('files', 'users.avatar_id', 'files.id')
          .first()
      );

      user.password = undefined;

      res.status(201).send(user);
    } catch (error) {
      res.send(error);
    }
  };

  const update = async (req, res) => {
    const { name, email, old_password, password, admin, avatar_id } = req.body;

    const loggedUser = await app
      .db('users')
      .where('id', req.user.id)
      .first();

    if (password && !old_password)
      return res
        .status(401)
        .send('Você precisa informar sua senha atual para atualizar a mesma.');

    if (
      old_password &&
      !(await bcrypt.compare(old_password, loggedUser.password))
    )
      return res.status(401).send('Sua senha atual está incorreta!');

    if (email && email !== loggedUser.email) {
      const emailAlreadyUsed = await app
        .db('users')
        .where({ email })
        .first();

      if (emailAlreadyUsed)
        return res.status(400).send('O e-mail já está em uso!');
    }

    const hashed_password = password && (await bcrypt.hash(password, 10));

    try {
      await app
        .db('users')
        .where('id', req.user.id)
        .update({ name, email, password: hashed_password, admin, avatar_id });

      const user = await nest.type(userSchema).withQuery(
        app
          .db('users')
          .select('*')
          .where('users.id', req.user.id)
          .leftJoin('files', 'users.avatar_id', 'files.id')
          .first()
      );

      user.password = undefined;

      res.send(user);
    } catch (error) {
      res.send(error);
    }
  };

  return { signUp, update };
};
