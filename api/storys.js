module.exports = app => {
  const store = async (req, res) => {
    const { name, status, sprint_id } = req.body;
    const { project_id } = req.params;

    try {
      const story = await app
        .db('story')
        .insert({
          name,
          status,
          project_id,
          sprint_id,
          user_id: req.user.id,
        })
        .returning('*');

      return res.send(story[0]);
    } catch (err) {
      return res.status(400).send('Falha ao incluir no banco de dados');
    }
  };

  const index = async (req, res) => {
    const { project_id } = req.params;

    try {
      const story = await app.db('story').where({ project_id });

      return res.send(story);
    } catch (err) {
      return res.status(400).send('Falha na consulta ao banco');
    }
  };

  const show = async (req, res) => {
    const { story_id, project_id } = req.params

    try {
      const story = await app.db('story').where({ id: story_id, project_id })

      return res.send(story)
    } catch (err) {
      return res.status(400).send(err)
    }

  }

  const update = async (req, res) => {
    const { name, status, sprint_id } = req.body;
    const { project_id, story_id } = req.params;

    const storyExist = await app
      .db('story')
      .where({ id: story_id })
      .first();

    if (!storyExist) {
      return res.status(404).send('Historia não existe');
    }

    try {
      const story = await app
        .db('story')
        .returning('*')
        .where({ id: story_id })
        .andWhere({ project_id })
        .update(
          {
            name,
            status,
            sprint_id,
          },
          '*'
        );
      return res.send(story);
    } catch (err) {
      return res.status(400).send('Falha ao incluir no banco de dados');
    }
  };

  const destroy = async (req, res) => {
    const { project_id, story_id } = req.params;

    const storyExists = await app
      .db('story')
      .where({ id: story_id })
      .first();

    if (!storyExists) {
      return res.status(404).send('Historia Não existe');
    }

    try {
      await app
        .db('story')
        .where({ project_id })
        .andWhere({ id: story_id })
        .del();

      return res.send('Historia deletada com Sucesso');
    } catch (err) {
      return res.status(400).send(err);
    }
  }

  return { store, index, show, update, destroy }
}
