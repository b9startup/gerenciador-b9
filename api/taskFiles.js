const { port } = require('../.env')

module.exports = app => {
    const store = async (req, res) => {
        const { task_id } = req.params
        const user_id = req.user.id

        try {
            const files = req.files.map(async file => {
                await app.db('task_files').insert({
                    task_id: task_id,
                    user_id: user_id,
                    original_name: file.originalname,
                    name: file.filename
                })
            })

            return res.send(files)

        } catch (err) {
            return res.status(400).send(err)
        }
    }

    const index = async (req, res) => {
        const { project_id, task_id, story_id } = req.params

        try {
            const filesUpload = await app.db('task_files').select('*').where({ task_id: task_id })


            filesUpload.forEach(file => {
                file.url = `http://localhost:${port}/v1/projeto/${project_id}/historia/${story_id}/tasks/${task_id}/files/${file.name}`
            })

            return res.send(filesUpload)


        } catch (err) {
            return res.status(400).send(err)
        }
    }

    const destroy = async (req, res) => {
        const { file_id } = req.params
        const user_id = req.user.id

        try {
            await app.db('task_files').where({ id: file_id, user_id: user_id }).del()

            return res.send("ok Deletado")
        } catch (err) {
            return res.status(400).send(err)
        }
    }

    return { store, index, destroy }
}