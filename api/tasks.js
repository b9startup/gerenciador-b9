const nest = require('nested-knex')
const tasksShow = require('../nestSchemas/tasksShow')
const tasksIndex = require('../nestSchemas/tasksIndex')

module.exports = app => {
    const store = async (req, res) => {
        const { story_id, project_id } = req.params;
        const {
            name,
            user_id,
            expected_start_date,
            expected_end_date,
            expected_hours,
            description
        } = req.body

        try {
            const current_sprint = await app
                .db('sprints')
                .select('sprints.id')
                .where({ project_id })
                .andWhere({ current_sprint: true })

            const sprint_id = current_sprint[0].id

            const task = await app
                .db('tasks')
                .returning('*')
                .insert({
                    name,
                    situation: 'Nova',
                    user_id,
                    story_id,
                    sprint_id,
                    expected_start_date,
                    expected_end_date,
                    expected_hours,
                    description
                })

            return res.send(task)

        } catch (err) {
            return res.status(400).send(err)
        }

    }

    const index = async (req, res) => {
        const { story_id } = req.params


        try {

            const tasks = await nest.array(nest.type(tasksIndex)).withQuery(
                app
                    .db('tasks')
                    .where({ ['tasks.story_id']: story_id })
                    .leftJoin('users', 'tasks.user_id', 'users.id')
            )

            return res.send(tasks)

        } catch (err) {
            return res.send(err)
        }

    }

    const show = async (req, res) => {
        const { story_id, task_id } = req.params

        try {
            const tasks = await nest.type(tasksShow).withQuery(
                app
                    .db('tasks')
                    .where({ ['tasks.id']: task_id, ['tasks.story_id']: story_id })
                    .select('*')
                    .leftJoin('users', 'tasks.user_id', 'users.id')
                    .leftJoin('comments', 'tasks.id', 'comments.task_id')
                    .leftJoin('users as users_assigned', 'comments.user_id', 'users_assigned.id')
                    .leftJoin('task_files', 'tasks.id', 'task_files.task_id')
            )




            return res.send(tasks)

        } catch (err) {
            return res.status(400).send(err)
        }

    }

    const update = async (req, res) => {
        const { story_id, task_id, project_id } = req.params
        const {
            name,
            situation,
            user_id,
            sprint_id,
            expected_start_date,
            expected_end_date,
            expected_hours,
            description
        } = req.body

        try {
            let start_date
            let end_date

            const realSituation = await app.db('tasks')
                .select('situation')
                .where({ id: task_id, story_id })



            const currentSituation = realSituation[0].situation



            if (situation && (situation != "Nova") && (currentSituation == "Nova")) {
                start_date = new Date()

                await app
                    .db('tasks')
                    .where({ id: task_id, story_id })
                    .update({ start_date, situation })
            }


            if (((situation == "Aprovado") || (situation == "Reprovado")) &&
                (currentSituation != situation)) {

                end_date = new Date()


                const hours_start = await app
                    .db('tasks')
                    .select('start_date')
                    .where({ id: task_id, story_id })

                const timeSpent = end_date - hours_start[0].start_date
                const timeSpentdInMinuts = timeSpent / (1000 * 60)
                const hoursSpent = (timeSpentdInMinuts / 60).toFixed(0)
                const minutsSpent = (timeSpentdInMinuts % 60).toFixed(0)
                const timeFinish = (`${hoursSpent}, ${minutsSpent}`)



                await app
                    .db('tasks')
                    .where({ id: task_id, story_id })
                    .update({ end_date, time_spent: timeFinish, situation })

            }

            if (sprint_id) {
                currentSprint = await app.db('tasks').select('sprint_id').where({ id: task_id })

                await app.db('task_history').insert({ id_task: task_id, old_sprint: currentSprint[0].sprint_id, new_sprint: sprint_id })
            }






            const task = await app
                .db('tasks')
                .returning('*')
                .where({ id: task_id, story_id })
                .update({
                    name,
                    situation,
                    user_id,
                    sprint_id,
                    expected_start_date,
                    expected_end_date,
                    expected_hours,
                    description
                })


            return res.send(task)

        } catch (err) {
            return res.status(400).send(err)
        }

    }

    const destroy = async (req, res) => {

        const { story_id, task_id } = req.params

        try {
            await app
                .db('tasks')
                .where({ id: task_id, story_id })
                .del()

            return res.send("Deletado com Sucesso")
        } catch (err) {
            return res.status(400).send(err)
        }



    }


    return { store, index, show, update, destroy }
}