const app = require('express')();
const consign = require('consign');
const db = require('./config/db');

app.db = db;

consign()
  .include('./config/passport.js')
  .then('./config/middlewares.js')
  .then('./config/multer.js')
  .then('./api/validation.js')
  .then('./api')
  .then('./validators')
  .then('./middlewares')
  .then('./config/routes.js')
  .then('./jobs')
  .then('./config/cron.js')
  .into(app);

app.listen(4000, () => {
  console.log('Backend executando...');
});
