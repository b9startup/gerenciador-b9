
exports.up = function (knex) {
    return knex.schema.createTable('task_files', table => {
        table.increments();
        table.string('name').notNullable();
        table.string('original_name').notNullable();
        table.string('url')
        table
            .integer('task_id')
            .references('id')
            .inTable('tasks')
            .onUpdate('CASCADE')
            .onDelete('CASCADE')
        table
            .integer('user_id')
            .references('id')
            .inTable('users')
            .onUpdate('CASCADE')
            .onDelete('SET NULL')
        table.timestamps(true, true)
    })

};

exports.down = function (knex) {
    return knex.schema.dropTable('task_files')

};
