exports.up = function(knex) {
  return knex.schema.alterTable('projects', table => {
    table.dropForeign('requester_id');
    table
      .foreign('requester_id')
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE')
      .onDelete('SET NULL');
  });
};

exports.down = function(knex) {
  return knex.schema.alterTable('projects', table => {
    table.dropForeign('requester_id');
    table
      .foreign('requester_id')
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE')
      .onDelete('CASCADE');
  });
};
