exports.up = function(knex) {
  return knex.schema.alterTable('users', table => {
    table
      .boolean('admin')
      .notNullable()
      .defaultTo(false);
    table.timestamps(true, true);
  });
};

exports.down = function(knex) {
  return knex.schema.alterTable('users', table => {
    table.dropColumn('admin');
    table.dropColumn('created_at');
    table.dropColumn('updated_at');
  });
};
