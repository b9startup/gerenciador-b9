exports.up = async function(knex) {
  await knex('files').insert({
    id: 1,
    name: 'avatar_placeholder.jpg',
    original_name: 'avatar_placeholder.jpg',
  });

  return knex.schema.alterTable('users', table => {
    table
      .integer('avatar_id')
      .defaultTo(1)
      .references('id')
      .inTable('files')
      .onUpdate('CASCADE')
      .onDelete('SET DEFAULT');
  });
};

exports.down = async function(knex) {
  await knex.schema.alterTable('users', table => {
    table.dropColumn('avatar_id');
  });

  return knex('files')
    .where('id', 1)
    .del();
};
