exports.up = function(knex) {
  return knex.schema.alterTable('story', table => {
    table
      .integer('sprint_id')
      .references('id')
      .inTable('sprints')
      .onUpdate('CASCADE')
      .onDelete('SET NULL');
  });
};

exports.down = function(knex) {
  return knex.schema.alterTable('story', table => {
    table.dropColumn('sprint_id');
  });
};
