
exports.up = function (knex) {
    return knex.schema.createTable('task_history', table => {
        table.increments();
        table
            .integer('id_task')
            .references('id')
            .inTable('tasks')
            .onUpdate('CASCADE')
            .onDelete('CASCADE')
            .notNullable()
        table
            .integer('old_sprint')
            .references('id')
            .inTable('sprints')
            .onUpdate('CASCADE')
            .onDelete('SET NULL')
            .notNullable()
        table
            .integer('new_sprint')
            .references('id')
            .inTable('sprints')
            .onUpdate('CASCADE')
            .onDelete('SET NULL')
            .notNullable()
        table.timestamps(true, true)

    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('task_history')

};
