exports.up = function (knex) {
    return knex.schema.createTable('story', table => {
        table.increments();
        table.string('name').notNullable();
        table
            .integer('user_id')
            .references('id')
            .inTable('users')
            .onUpdate('SET NULL')
            .onDelete('SET NULL');
        table
            .integer('project_id')
            .references('id')
            .inTable('projects')
            .onUpdate('CASCADE')
            .onDelete('CASCADE')
        table.string('status');
        table.timestamps(true, true);
    });
};

exports.down = function (knex) {
    return knex.schema.createTable('story')
};