exports.up = function(knex) {
  return knex.schema.createTable('projects', table => {
    table.increments();
    table.string('name').notNullable();
    table.string('description');
    table
      .date('starting_date')
      .notNullable()
      .defaultTo(knex.fn.now());
    table.date('expected_end_date').notNullable();
    table.date('delivery_date');
    table.string('expected_hours').notNullable();
    table.string('time_spent');
    table.string('priority').notNullable();
    table
      .integer('requester_id')
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE')
      .onDelete('CASCADE');
    table.string('requirements');
    table.string('status');
    table.timestamps(true, true);
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('projects');
};
