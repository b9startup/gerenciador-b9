exports.up = function(knex) {
  return knex.schema.alterTable('projects', table => {
    table
      .integer('sprint_days_interval')
      .unsigned()
      .notNullable()
      .defaultTo(15)
      .after('status');
  });
};

exports.down = function(knex) {
  return knex.schema.alterTable('projects', table => {
    table.dropColumn('sprint_days_interval');
  });
};
