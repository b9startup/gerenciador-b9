exports.up = function(knex) {
  return knex.schema.createTable('files', table => {
    table.increments();
    table.string('name').notNullable();
    table.string('original_name').notNullable();
    table.timestamps(true, true);
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('files');
};
