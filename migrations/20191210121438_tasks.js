
exports.up = function (knex) {
    return knex.schema.createTable('tasks', table => {
        table.increments();
        table.string('name').notNullable();
        table
            .enu('situation',
                [
                    'Nova',
                    'Em Analise',
                    'Desenvolvimento',
                    'Parado',
                    'Aguardando Testes',
                    'Testando',
                    'Aprovado',
                    'Reprovado'
                ],
                { useNative: true, enumName: 'Situation' }).notNullable();
        table
            .integer('user_id')
            .references('id')
            .inTable('users')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
        table
            .integer('story_id')
            .references('id')
            .inTable('story')
            .onUpdate('CASCADE')
            .onDelete('CASCADE')
            .notNullable();
        table
            .integer('sprint_id')
            .references('id')
            .inTable('sprints')
            .onUpdate('CASCADE')
            .onDelete('SET NULL')
            .notNullable();
        table.date('expected_start_date').notNullable();
        table.date('start_date');
        table.date('expected_end_date').notNullable();
        table.date('end_date');
        table.string('expected_hours').notNullable();
        table.string('time_spent')
        table.string('description').notNullable();
        table.timestamps(true, true);
    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('tasks')

};
