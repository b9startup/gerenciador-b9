
exports.up = function (knex) {
    return knex.schema.createTable('comments', table => {
        table.increments();
        table
            .integer('task_id')
            .references('id')
            .inTable('tasks')
            .onUpdate('CASCADE')
            .onDelete('CASCADE')
            .notNullable();
        table
            .integer('user_id')
            .references('id')
            .inTable('users')
            .onUpdate('CASCADE')
            .onDelete('CASCADE')
            .notNullable();
        table.text('comments')
            .notNullable();
        table.timestamps(true, true)
    })

};

exports.down = function (knex) {
    return knex.schema.dropTable('comments')

};
