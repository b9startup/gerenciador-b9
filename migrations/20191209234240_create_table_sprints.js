exports.up = function(knex) {
  return knex.schema.createTable('sprints', table => {
    table.increments();
    table.string('name').notNullable();
    table
      .integer('project_id')
      .references('id')
      .inTable('projects')
      .onUpdate('CASCADE')
      .onDelete('SET NULL');
    table
      .boolean('current_sprint')
      .notNullable()
      .defaultTo(false);
    table
      .date('starting_date')
      .notNullable()
      .defaultTo(knex.fn.now());
    table.date('expiration_date').notNullable();
    table.timestamps(true, true);
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('sprints');
};
