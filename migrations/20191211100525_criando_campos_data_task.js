
exports.up = function (knex) {
    return knex.schema.alterTable('tasks', table => {
        table.timestamp('start_date', { useTz: true });
        table.timestamp('end_date', { useTz: true })
    })

};

exports.down = function (knex) {
    return knex.schema.alterTable('tasks', table => {
        table.dropColumn('start_date');
        table.dropColumn('end_date')
    })

};
