module.exports = app => {
  const checkMembers = async (req, res, next) => {
    if (req.user.admin) return next();

    const { project_id, id } = req.params;

    const isMemberOfProject = await app
      .db('project_members')
      .where('project_id', project_id || id)
      .andWhere('user_id', req.user.id)
      .first();

    if (!isMemberOfProject) {
      return res.status(401).send('Usuario não pertence a esse projeto');
    }

    next();
  };
  return checkMembers;
};
