module.exports = app => {
  const checkAdmin = (req, res, next) => {
    if (!req.user.admin)
      return res
        .status(401)
        .send('Você não tem permissão para realizar esta ação.');

    next();
  };

  return checkAdmin;
};
