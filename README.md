# Gerenciador B9

Olá, seja bem vindo(a) ao nosso repositório do Gerenciador B9. Esse repositório é responsável pelo Back-end da aplicação.

# Como executar o back-end?

Após clonar o respositório você receverá entrar na pasta do projeto e executar o comando: 

<blockquote>
  npm install
</blockquote>

Feito isso basta executar o projeto com o comando: 

<blockquote>
  npm start
</blockquote>

Seu back-end deverá ser executado na porta 4000 (Padrão, caso não mude a porta)

<i>Versão 1.0.1</i>